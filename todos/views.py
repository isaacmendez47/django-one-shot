from django.shortcuts import render , get_object_or_404
from todos.models import TodoList

# Create your views here.

# list view(mainpage)
def todo_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list" : todo_list
    }
    return render(request, "todos/todos.html", context)

# detail view(views tasks)
def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        'todo_list_object': todo_list,
    }
    return render(request, "todos/detail.html", context)

# this one might need a change in the second argmuent to
# a different template route route
